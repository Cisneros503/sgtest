using System;
using System.Collections.Generic;


namespace TestApi.Models
{
    public class BeJob{

        public int Job { get; set; }
        public string JobTitle { get; set; } 
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ExpiresAt { get; set; }

        BeJob(int job, string jobTitle, string description, DateTime createdAt, DateTime expiresAt){
            
            this.Job = job;
            this.JobTitle = jobTitle;
            this.Description = description;
            this.CreatedAt = createdAt;
            this.ExpiresAt = expiresAt;
            
        }

        BeJob(){
            this.Job = -1;
            this.JobTitle = String.Empty;
            this.Description = String.Empty;
            this.CreatedAt = new DateTime(1900, 01, 01, 0, 0, 0);
            this.ExpiresAt = new DateTime(1900, 01, 01, 0, 0, 0);
        }

        
    }
}

